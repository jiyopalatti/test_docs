Computing Display Unit (CDU)
============================

The user interface for the display unit consists of a display panel and
a total of eight buttons with tactile feedback (2) on both sides of the
screen. The upper left corner features a light sensor (1), which adjusts
screen brightness automatically when the brightness adjustment (BR) is
in AUTO mode.

The display User Interface (UI) colours have the following meanings:

+-----------------------+-----------------------------------------------------------+ 
|       Colour          |     Legend                                                |
+=======================+===========================================================+
|     Blue (cyan)       |   Function ready for operation, or static text            |
+-----------------------+-----------------------------------------------------------+
|     Green             |   Active function, indication, status normal              |
+-----------------------+-----------------------------------------------------------+
|     Violet (magenta)  |   Locked reference value, target heading or position      |
+-----------------------+-----------------------------------------------------------+
|     Orange (amber)    |   Caution, malfunction or failure                         |
+-----------------------+-----------------------------------------------------------+


Changing display mode
---------------------

.. raw:: html

    <embed>
        <img src="../_static/images/cdu/disp_button.png" width="50%" height="50%" />
    </embed>

The display mode can be changed with the DISP button. The system has
three separate display modes:

-  Propulsion (P)
-  Navigation (N)
-  Diagnostics (D)

Press the DISP button to change between the views mentioned above. The
diagram below describes the order in which the display modes are
changed.

Add tip and note.

Propulsion view (P)
-------------------

The Propulsion view is opened by default when the system starts up. In
the Propulsion view, you can monitor the operation of the control
devices. A two-jet system is described below.

+------+---------------------------------------------------------------+
| No   | Description                                                   |
+======+===============================================================+
| 1    | N1, tachometers for both jet impellers r/min                  |
+------+---------------------------------------------------------------+
| 2    | DEF, graphical representation of reversing deflector position |
|      | (down-center-up)                                              |
+------+---------------------------------------------------------------+
| 3    | NOZ, graphical representation of steering nozzle position in  |
|      | degrees (left-center-right)                                   |
+------+---------------------------------------------------------------+
| 4    | Command value (requested position) indicator                  |
+------+---------------------------------------------------------------+

Navigation view (N)
-------------------

The Navigation compass view is activated when the DISP button is pressed
in the Propulsion view. This view shows a true heading compass circle
with own vessel indicator in the middle. The current location (lat, lon)
is shown below

Diagnostics view (D)
--------------------

In the Diagnostics view, you can see graphical representations of
certain critical control system functions. The options are hydraulics,
electrical system and error log.

By pressing the DISP button, you can navigate via the Propulsion view
(1) and Navigation view (2) to the Diagnostics view, where you can
switch between the pages for hydraulics (3, HYD), electrical system (4,
ELEC) and error log (5, LOG).

To change to another page, use the buttons in the upper right corner of
the display.

If there is a problem in any of these areas, the button identifiers will
be outlined in orange.

In the diagram, orange frame indicates a failure. Feedback value is
exceeding the system’s limit values or the value is not available (xx).

Hydraulic system diagnostics view (HYD)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The hydraulics view is divided into the starboard (STBD) and port (PORT)
section (standard twin installation), where the revolutions per minute
(N1 / rpm) are shown on top, followed by the oil pressure (P1 /bar) and
temperature 1 (T1 / °C) at the bottom. In the example figure below, the
system has a problem on the starboard side (the fluid level in the
hydraulic tank is low).

Electrical system diagnostic view (ELEC)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The electrical system view displays the different units’ power supply
voltage status (V) in the propulsion buses (Primary Bus) and auxiliary
bus (AUX) in real time. The green color indicates sufficient power
supply for each unit. In typical configurations JCU voltage levels are
indicating slighly lower levels than HCU caused by higher power
consumption at the engine compartment end. Also engaging gearbox can add
an additional voltage drop to JCU end. Both drivelines should indicate
symmetrical behaviour in voltage levels in normal operation.

The diagram units are as follows:

== =============================================================
No Description
== =============================================================
1  Auxiliary Bus (AUX) supply voltage at the position of the CDU
2  Helm control units (HCU)
3  Power supply points DC1, DC2, DC3
4  Jet control unit (JCU) voltage
== =============================================================

Log view (LOG)
~~~~~~~~~~~~~~

The log view displays a summary of all failures in time stamp order with
the newest at the top. If there is no active errors (orange triangle),
the orange symbols will be cleared out after visiting LOG page. Full
list of possible error codes is listed in Troubleshooting section.

Brightness adjustment
---------------------

Select automatic adjustment for the control device brightness or one of
the three manual brightness settings by pressing the BR button. When
automatic brightness adjustment is on, the text AUTO is shown under the
BR indicator. The brightness control covers also the control heads.

Idle adjustment
---------------

It is sometimes a good idea to keep the idle setting at a high level to
maintain the sufficient handling of the vessel, especially in windy
conditions. The virtual anchor may also require the idle rpm to be
increased in extreme conditions. Press the IDLE button to adjust the
idle level Three manual settings are available in addition to the
automatic option.

A small triangle indicates the idle ramp-up mode to be active. The idle
adjustment resets back to the auto mode if the button pressed again
after a delay more than one second (safety feature).

Gear control (optional)
-----------------------

Switch gears with the GEAR button. There is a separate GEAR button for
each gearbox. The text identifier in the display button and the clutch
symbol indicate whether or not the gear is engaged. Once the SIGMA
system has started, the gears might be engaged or disengaged by default
(commissioning parameter). Please check the vessel spesific instrcutions
for engine startup procedure. The options are as follows:

====== ===============
Symbol Status
====== ===============
N      Neutral
D      Drive (engaged)
BF     Backflush
====== ===============

Drive
~~~~~

Switch the transmission from neutral to drive by pressing GEAR (N). The
indication GEAR (D) will appear on screen.

Backflush model (BF)
~~~~~~~~~~~~~~~~~~~~

The purpose of the backflush mode is to clean the water intake opening
and tunnel by changing the flow direction of the water temporarily.

1. Switch the transmission from neutral to backflush by pressing GEAR
   (N) for about three seconds to activate backflush.

2. Keep the button pressed and backflush will stay engaged for a maximum
   of three seconds.

3. You can interrupt backflush before this by releasing the button. The
   system will then return to neutral (N) automatically.
