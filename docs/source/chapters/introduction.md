# Introduction
Congratulations on purchasing a new Alamarin-Jet SIGMA electronic propulsion control system! This manual contains important information on the operation and maintenance of the system. Please read these instructions carefully before using the system. This will ensure the safe operation of the system. 

 Please keep this manual available for the system’s entire life span. If you lose the manual, contact your nearest distributor for a new one. If you sell the system, make sure you hand over this manual to the new owner. Please contact your nearest distributor if you have any queries regarding installation or commissioning. 

© Alamarin-Jet Oy  
Tuomisentie 16  
FI-62300 Härmä, Finland  
+358 10 7745 260  
www.alamarinjet.com  

All rights reserved. The information in this manual may not be copied, published or reproduced in any way whatsoever, or exploited for commercial purposes, without express written permission from Alamarin-Jet Oy. 

Modifying the equipment, factory settings or other features of the vessel may be detrimental to its performance. No such changes are allowed without permission from the appropriate party (manufacturer of the vessel, control system or other element). 

The information in this manual is subject to change without notice. The screen captures in this manual are for reference only, and the actual views may vary depending on the system assembly. Alamarin-Jet Oy reserves the right to modify the contents without notice. 

## Safety

<img src="../_static/images/symbols/note.png" width="5%" height="5%" />

- As the driver, you are responsible for the appropriate and safe use of the vessel and equipment and the safety of others onboard. Please read this manual and other necessary operating, maintenance and warranty instructions to understand the operation of the engine and other equipment before using the vessel.

- Wind, waves and load will make the vessel behave differently from the ideal conditions on which the instructions in this manual are based. Practice using the controls in varying conditions to learn how to compensate for the impacts of the conditions with corrective motions.

- Please adhere to the safety instructions provided in this manual at all times.

- The system may only be maintained by persons with the appropriate training.

- Effective protective equipment must be used for maintenance activities.

- The size, safety and lighting of the working area must be sufficient.

- The tools used must be appropriate.

## Abbreviations and terms

|     Term                     |   Meaning
|------------------------------|--------------------------------------------------------------------------------------------------------------------------------|
|     3CU                      |   Joystick Control Unit                                                                                                        |
|     3XJS                     |   Three-axis Joystick                                                                                                          |
|     AUX                      |   Auxiliary Bus                                                                                                                |
|     AMOC                     |   Analog Motor Controller Unit                                                                                                 |
|     ANC                      |   Intelligent Dynamics Virtual Anchor                                                                                          |
|     BAT                      |   Power source (battery)                                                                                                       |
|     CAN                      |   Controller Area Network (local digital communicationg bus standard)                                                          |
|     CDU                      |   Computing Display Unit                                                                                                       |
|     DEF                      |   Reversing Deflector, a control surface to direct the jet output thrust forward backward	                                |
|     DPS                      |   Intelligent Dynamics Position Hold (Dynamic Positioning)                                                                     |
|     ECU                      |   Engine Control Unit                                                                                                          |
|     EHL                      |   Electronic Helm Unit                                                                                                         |
|     HCU                      |   Helm Control Unit                                                                                                            |
|     HDG                      |   Intelligent Dynamics Heading Keeping (Autopilot)                                                                             |
|     J1939                    |   CAN standard for rngine bus control                                                                                          |
|     JCU                      |   Jet Control Unit                                                                                                             |
|     N1                       |   Jet impeller rotation speed (rpm) 	                                                                                        |
|     NMEA2000                 |   CAN control standard for vessel electronics and navigation systems                                                           |
|     NOZ                      |   Nozzle, a control surface to direct the jet output thrust left or right	                                                |
|     Main control system      |   The primary control system assembly without accessories                                                                      |
|     Optional extensions      |   Optional components to be connected to the main control system                                                               |
|     P1                       |   Hydraulic pressure measured from the valve block	                                                                        |
|     PB                       |   Primary Bus                                                                                                                  |
|     SOG                      |   Speed Over Ground	                                                                                                        |
|     T1                       |   Hydraulic fluid temperature	                                                                                                |
|     TWL                      |   Twin Levers unit for propulsion control                                                                                      |


Please note that these instructions use the terms ‘jet’ and ‘jet propulsion unit’.They refer to the same thing.

## Symbols

Please refer to table 2 for a description of the symbols used in this manual. 
| Icon | Description |
| ---- | ----------- |
|![](../_static/images/symbols/danger.png)| **DANGER** Negligence in the performance of the procedure can endanger your life.   |
|![](../_static/images/symbols/warning.png)| **WARNING** Negligence in the performance of the procedures can lead to personal injury, breakdown of equipment, or serious malfunction of the equipment.   |
|![](../_static/images/symbols/caution.png)| **CAUTION** The procedure involves minor danger or a possibility of minor damage to equipment.   |
|![](../_static/images/symbols/warranty.png)| **WARRANTY** The warranty is voided if the procedure is carried out incorrectly.   |
|![](../_static/images/symbols/note.png)|  **NOTE** Important notice or fact.   |
|![](../_static/images/symbols/tip.png)|  **TIP** Additional information that facilitates the performance of work ora procedure.   |
