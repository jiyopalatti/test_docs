# Power supply requirements

The system’s power supply requirements are as follows:
| Bus              | Requirement (A, V) |
|------------------|--------------------|
| Propulsion buses | 8 A, 12/24 V       |
| Auxiliary bus    | 4 A, 12/24 V       |
| Sensor bus       | 4 A, 12/24 V       |

Add info about redudancy!!