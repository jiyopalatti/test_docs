# Operating modes


## Normal drive mode
In normal drive mode, the boat is controlled with the steering wheel (EHL)
and twin control levers (TWL). In the base system when using at least two jets,
the left lever controls the direction and level of port propulsion while the right 
controls starboard propulsion. In the event of malfunctions, the double redundant system 
enables you to keep driving using the functional side without additional mode changes. 
The control levers can also be separated so that one controls propulsion force
while the other controls the jet reversing deflectors (separated mode). 
This is the standard solution for single-jet systems.


## Three axis joystick (3XJS) drive mode
3XJS drive means a mode in which the vessel is controlled with one or more three-axis joysticks (one station at the time). 
The 3XJS mode can be used in the harbour, or anywhere where precise slow speed manouvering is required. 


## Intelligent Dynamics (ID) option
Systems with the enabled ID option will include the following additional
functions:
- Autopilot (HDG) — The vessel tries to keep the bow in the predefined
compass heading (true heading). The vessel can be operated at any speed.

- Dynamic Positioning System (DPS) — The control system tries to keep the
vessel’s position and heading locked. At least two jets are required for this
extra function. This feature can be activated only at slow speeds.

- Virtual Anchor (ANC) — When this mode is active, the bow of the vessel
points to a position at a specified distance, as if an anchor has been lowered
at that position. The vessel will balance itself bow against the wind (or other external forces).
This feature is more energy efficient than using DPS, but compromises position accuracy. 
This feature can be activated only at slow speeds.


## Limited operation in system malfunction situations
If some of the jets become clogged or damaged, or if a control system component malfunctions, in a system with two or more jets, 
the operational jets can be used to continue driving with certain limitations. Please refer to section Troubleshooting.  

The captain of the vessel must decide on further measures and consult experts if necessary.
Limitations in the case of a malfunction:
- The propulsion power of defected driveline may be limited, intermittent or resulting unwanted forces.

- Steering performance may be limited or one-sided.

- For safety reasons, do not use any other controls than the steering wheel
and levers at the main control station (primary control heads).

Practise driving a limited/one-sided system in case of failures or malfunctions. For
the purpose of practising, select an open water area with no traffic or
obstructions.
