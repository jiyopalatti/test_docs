# System components

## Control heads

<img src="../_static/images/control_heads/control_heads.png" width="75%" height="75%" />


|       No  |     Description                 |
|-----------|---------------------------------|
|     1     |   Computing Display Unit (CDU)  |
|     2     |   Electronic Helm (EHL)         |
|     3     |   Twin Control Levers (TWL)     |
|     4     |   Joystick (3XJS, accessory)    |

The figure above depicts the control devices for the electronic control system.
These controls can be found at the main control station. Another station
with the same controls (fully equipped station) can be installed as an accessory, as well as additional
control stations with joysticks and CDUs. The vessel can feature up to four stations.


## Engine room

<img src="../_static/images/diagrams/components_engine_room.png" width="75%" height="75%" />

The picture above shows a typical engine room configuration for a single driveline. Please note the configuration might look different for each jet model. Each jet unit works as an independent CAN bus driven propulsion device. The control messages are passed via Propulsion Bus to the Jet Control Unit (JCU) (4). The JCU takes care of all the control loops required to operate the jet unit and the engine (5). If the driveline is equipped with a transmission unit, control signals (3) are typically taken out from the JCU (4). Engine RPM is typically controlled using engine's own CAN bus (J1939) (2). If the CAN control is not possible or allowed, an additional Analog Motor Controller (AMOC) unit may be used. The JCU also monitors Jet unit specific parameters e.g. hydraulic oil pressure and tanks' (7, 8) oil levels. The available points of feedback may vary in different configurations and should be consider system spesific. 

|       No  |     Description                                          |
|-----------|----------------------------------------------------------|
|     1     |   Primary Bus (Separate Bus for each driveline)          |
|     2     |   Engine Control Bus (J1939)                             |
|     3     |   Gear control (FW/BW, the marine gear is an accessory)  |
|     4     |   JCU, Jet Control Unit                                  |
|     5     |   ECU, Engine Control Unit                               |
|     6     |   Transmission / Gear (accessory)                        |
|     7     |   Oil tank (HYD, hydraulics)                             |
|     8     |   Oil tank (BEAR, front bearing lubrication)             |
