# Steering

The vessel is steered by means of the steering wheel, control levers and joystick (accessory).

## Electronic steering wheel (EWH)

The electronic steering wheel turns the jet nozzle when the vessel is moving forward and therefore turns the vessel in the direction in which the wheel is turned. 
Depending on the control method, the steering may require a sufficient rpm to function properly.

## Twin control levers (TWL)

FIGURE 18

ADD NOTE

Table. : Controlling the jets with the control levers
|       Number   |     Lever position             |     Deflector position                        |     Description                                                                                                                            |     Rpm    |
|----------------|--------------------------------|-----------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|------------|
|     1          |   Full fordward position       |   Top position ADD PIC                        |   Direct forward acceleration                                                                                                              |   Maximum  |
|     2          |   +20                          |   Top position     ADD PICS                   |   Direct forward acceleration                                                                                                              |   Minimum  |
|     3          |   Middle position (´neutral´)  |   Lowered to approx. mid position    ADD PIC  |   The middle position of the control device, which must  be engaged when starting the system, using gears and switching to the 3XJS mode.  |   Minimum  |
|     4          |   -20                          |   Bottom position    ADD PIC                  |   Slowing down or reversing                                                                                                                |   Minimum  |
|     5          |   Full reverse position        |   Bottom position     ADD pic                 |   Slowing down or reversing                                                                                                                |   Maximum  |

## Steering the vessel with the steering wheel and control levers

Practice using the steering wheel and control levers simultaneously in a safe water area.
Please note that the sensitivity of the steering wheel and control levers is affected by factors such as the number of jets used and the physical properties of the vessel, in addition to the conditions.
The idling speed can be increased by pressing the IDLE button in the Propulsion view.

ADD FIGURE 19.

## 3XJS joystick (accessory)

ADD FIGURE 20.

You can use the joystick for precise one-handed control of the vessel.

In autopilot mode (HDG), you can momentarily correct the boat’s course with the 3XJS joystick.
For more information, see section 9.3. Autopilot (HDG), page 33.
There can be up to four additional control stations in varying assemblies.

### Using the joystick at the main control station
1. Move both control levers to neutral (middle position) and the steering
wheel to the middle position.

2. Move the joystick in the direction you want the vessel to move in or rotate it in the direction you want the vessel to turn towards.
You can move and rotate the joystick at the same time.

3. Basic joystick movements:

ADD FIGURE 21.

The further from the middle you move the joystick, the stronger its effect will be.

### Changing the control station
You install multiple joystick control stations in the same vessel. Each control station can have additional controls alongside the joystick.
To activate or deactivate the joystick, do the following:

1. Press the button at the end of the joystick (1).

2. Go to the desired control station.

3. Press the button (1) at the end of the auxiliary control station’s joystick
for at least one second. The light ring (2) stays on (activation) or begins to
pulse (deactivation).

DANGER
Losing control of the vessel may result in severe injuries or even death.
When the vessel is in motion and the engine gear is engaged, you must never leave the control station you are using.
When the vessel is moving, you may only change control stations if there is a driver at both stations.
When you are alone, do not switch to another control station unless the vessel is stationary.

DANGER
The control levers must be centred when changing control station.
This means that the vessel may drift and collide with an obstacle.
Exercise caution when switching control stations, especially when the vessel is near other vessels or obstacles.
