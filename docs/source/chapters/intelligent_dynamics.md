# Intelligent dynamics (additional feature)

In systems with GPS positioning, certain additional features can be used in the Propulsion (P) and Navigation (N) modes.
These features include the virtual anchor (ANC), dynamic positioning (DPS) and autopilot (HDG).

ADD NOTE

ADD CAUTION

## ANC - Virtual anchor

ADD PIC

When the ANC function is active, the bow of the vessel points to a position at a specified distance, as if an anchor has been lowered at that position
The vessel’s position may change as a result of the wind or other causes, but the bow will still be kept towards this predefined point and at the set distance from it.
The default distance between the vessel’s GPS antenna and virtual anchor is 15 m.

ADD FIGURE 24.

| No | Description                             |
|----|-----------------------------------------|
| 1  | Furthest limit for vessel motion (15 m) |
| 2  | Intelligent vessel anchor location      |
| 3  | Bow direction towards the anchor        |

1. In order to anchor the vessel, press ANC in the Propulsion (P) or Navigation (N) mode.
The indicator shows the text AUTO and the distance between the vessel’s GPS antenna and the virtual anchor point.

ADD PIC AND TIP

2. To switch back to the STBY mode, press the ANC button again.

ADD PIC

## DPS - Dynamic positioning

ADD PIC

The GPS signal is used to lock the vessel in place. 
This function is particularly useful when you are waiting for a bridge to open, your turn to fill your fuel tank or access to a pier, or when the water is too deep for a normal anchor.

ADD FIGURE 25

ADD DANGER SYMBOL AND BOX

In DPS mode, the jets and engine are operational and the vessel is considered to be in motion. 
This may pose serious danger to people in the water near the vessel.
Even with DPS mode engaged, the driver is still responsible for controlling the vessel and monitoring the conditions.
If there are people or other boats in the water nearby, disengage the function immediately and turn off the engines to prevent injuries and property damage.

- Tell the people onboard how the DPS function works and instruct them to stay out of the water or any places from which they can easily fall overboard.
In case of sudden movements of the vessel, persons onboard must hold on to a railing or other fixed element.

- Instruct those onboard about the vessel’s warning sounds or indicators and when they may be activated.

- Ensure that no one is in the water near the vessel.

- Stay near the steering wheel and keep an eye on the surroundings.

- Switch the DPS off immediately if you see anything in the water near the
vessel.

Activate DPS as follows:
1. Drive the vessel to a safe place of your choosing.

2. To keep the vessel in place, press DPS in the Propulsion (P) or Navigation (N) mode.
The vessel will now use the jets to stay in place as accurately as possible. 
The measurement of the deviation is shown in the bottom corner of the indication.
ADD PIC

3. The target position is shown in violet (magenta) in the middle of the compass view.
The arrow indicates the direction and level of the steering power.
ADD PIC

4. To switch back to STBY mode, press the same button again. 
Alternatively, the DPS can be switched off by moving the joystick or control levers or turning the steering wheel.
ADD PIC

FORMAT NOTE
The operation of the DPS is dependent on conditions. Mastering the correct positioning of the vessel takes practice.
In difficult conditions, especially if the vessel is perpendicular to the direction of the wind or current, it may be impossible to keep it fully in place.
However, the system will always try to align the bow with the original set point.

FORMAT NOTE
The GPS signal may be lost in obstructed areas, such as under a bridge or near tall structures.
If the GPS signal is lost, the DPS will be switched off, at which point you must be ready to assume control.

ADD FIGURE 26

## HDG - Autopilot

ADD PIC

In the HDG mode, the vessel tries to maintain the compass heading (true heading) automatically.
The set heading can be changed by rotating the joystick.

ADD FIGURE 27

ADD DANGER BOX
The driver is responsible for controlling the vessel even when autopilot is engaged.
In HDG mode, the vessel travels in the predefined direction, disregarding all possible obstacles or other vessels in its path.
This means that the driver must be ready to assume manual control to avoid danger.

Engage the autopilot as follows:

1. Ensure that the gear is engaged and the controls are in the middle position.

2. To lock the vessel’s travel direction, press HDG in the Propulsion (P) or Navigation (N) mode.
The vessel will continue travelling in the direction it was heading when the lock was engaged.
The locked heading (True Heading) is shown on the bottom row of the indicator.

3. If necessary, you can correct the vessel’s heading by activating the joystick and rotating it.
You can also navigate around obstacles by moving the joystick to the right or left.
When you release the joystick, the vessel will return to the predefined heading.

4. To switch back to STBY mode, press the same button again.
Alternatively, you can switch off the HDG function by moving the control levers as described above or otherwise, or by turning the steering wheel.

ADD PICS TO STEPS ABOVE

