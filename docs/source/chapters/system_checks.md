# System checks

## Checking the oil level
1. Check the oil level through the sight glass.
ADD FIG 28.

2. If the sight glass indicates that the oil level is below half capacity, checkthe dipstick (1) to see whether the oil level is between the maximum andminimum level markers (2).
ADD FIG 29.

3. Ensure that the oil type is suitable for the jet.

4. If there is too little oil, add oil gradually and check the dipstick occasionally during filling.