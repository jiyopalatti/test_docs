.. sigma-operation-manual documentation master file, created by
   sphinx-quickstart on Wed Sep 21 13:57:50 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sigma-operation-manual's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   chapters/introduction.md
   chapters/system_components.md
   chapters/operating_modes.md
   chapters/cdu.rst
   chapters/startup.md
   chapters/steering.md
   chapters/intelligent_dynamics.md
   chapters/power_supply_requirements.md
   chapters/system_checks.md
   chapters/failures_and_troubleshooting.md
   chapters/test.md

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
