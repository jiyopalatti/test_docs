# Alamarin-jet Documentation

Steps
* Clone the Git repository
* Install required packages
    * `sudo apt install texlive-latex-recommended texlive-fonts-recommended tex-gyre texlive-latex-extra latexmk`
* Create a Python virtual environment
    * `python -m venv .venv`
* Activate the python virtual environment
    * `source .venv/bin/activate`
* Install required packages
    * `pip install -r requirements.txt`
* Build the documentation for website deployment
    * `sphinx-build -b html docs/source/ docs/build/html`
OR

* Build the documentation as pdf
    * `cd docs/`
    * `make pdflatex`


